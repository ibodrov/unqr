pub mod parser {
    pub fn break_line<'a>(line: &'a str, sep: &str, quote: &str) -> Vec<&'a str> {
        enum State {
            NORMAL,
            QUOTE,
            DELIM,
        }

        let mut fields = Vec::new();
        let mut start_quote = ' ';
        let mut pos = 0;
        let mut field_start = 0;
        let mut state = State::NORMAL;

        for c in line.chars() {
            let quote_found = quote.contains(c);
            let delim_found = sep.contains(c);
            let mut flush_field = false;
            let mut empty_field = false;

            match state {
                State::NORMAL => {
                    if quote_found {
                        start_quote = c;
                        state = State::QUOTE;
                        flush_field = true;
                    } else if delim_found {
                        state = State::DELIM;
                        flush_field = true;
                    }
                }
                State::QUOTE => {
                    if quote_found && start_quote == c {
                        state = State::NORMAL;
                        flush_field = true;
                        empty_field = true; // dump empty field because quoted
                    }
                }
                State::DELIM => {
                    if quote_found {
                        state = State::QUOTE;
                        start_quote = c;
                        field_start = pos + 1;
                    } else if delim_found {
                        field_start = pos + 1; // simply eat all delims
                    } else {
                        state = State::NORMAL;
                        field_start = pos;
                    }
                }
            }

            if flush_field {
                if field_start != pos || empty_field {
                    fields.push(line.get(field_start..pos).unwrap()); // unwrap() because there's no way it can fail
                }

                field_start = pos + 1;
            }

            pos += 1;
        }

        if field_start < pos {
            fields.push(line.get(field_start..pos).unwrap());
        }

        fields
    }

    pub fn compose_line(fields: Vec<&str>, sep: &str) -> String {
        fields.join(sep)
    }

    pub fn process(sep: &str, quote: &str, delim: &str) {
        use std::io;
        use std::io::prelude::*;

        let stdin = io::stdin();
        for line in stdin.lock().lines() {
            let line_str = &line.unwrap();
            let fields = break_line(line_str, delim, quote);
            println!("{}", compose_line(fields, sep));
        }
    }

    #[cfg(test)]
    fn do_vecs_match<T: PartialEq>(a: &Vec<T>, b: &Vec<T>) -> bool {
        let matching = a.iter().zip(b.iter()).filter(|&(a, b)| a == b).count();
        matching == a.len() && matching == b.len()
    }

    #[cfg(test)]
    fn validate(input: &str, exp: &Vec<&str>) {
        let res = break_line(input, " ", "'");
        let m = do_vecs_match(&res, exp);
        if !m {
            println!("Mismatch {} <> {:?}", input, exp);
        }
        assert!(m);
    }

    #[cfg(test)]
    fn validate_quote(input: &str, exp: &Vec<&str>, q: &str) {
        let res = break_line(input, " ", q);
        let m = do_vecs_match(&res, exp);
        if !m {
            println!("Mismatch [{}] {:?} <> {:?}", input, res, exp);
        }
        assert!(m);
    }

    #[test]
    fn test_break() {
        validate("this is text", &vec!["this", "is", "text"]);
        validate("  this is text  ", &vec!["this", "is", "text"]);
        validate("this 'is' ''text", &vec!["this", "is", "", "text"]);
        validate(
            "this      'is'      ''  text           ",
            &vec!["this", "is", "", "text"],
        );
        validate_quote(
            "this      'is'      \"\"  text           ",
            &vec!["this", "is", "", "text"],
            "\"'",
        );
        validate_quote(
            "this      'is'      \"\"  text           ",
            &vec!["this", "'is'", "", "text"],
            "\"",
        );
        validate_quote(
            "this      'is'      \"\"  text           ",
            &vec!["this", "is", "", "text"],
            "\"'",
        );
        validate_quote(
            "this      \"'is'\"      \"\"  text           ",
            &vec!["this", "'is'", "", "text"],
            "\"'",
        );
    }
}

fn main() {
    use clap::{App, Arg, ArgGroup};

    let matches = App::new("unqr")
        .version("1.0")
        .author("paul romanchenko <paulanergmail.com>")
        .about("Split input lines into fields preserving quoted fields. \
                Join fields with given separator.")
        .arg(Arg::with_name("delimiter")
            .short("f")
            .takes_value(true)
            .help("Use given delimiter to split fields. Default space and tab"))
        .arg(Arg::with_name("single")
            .short("s")
            .long("single")
            .help("use single quote '")
            .takes_value(false))
        .arg(Arg::with_name("double")
            .short("d")
            .long("double")
            .help("use double quote \", this is default")
            .takes_value(false))
        .arg(Arg::with_name("both")
            .short("b")
            .long("both")
            .help("use both single and double quotes")
            .takes_value(false))
        .arg(Arg::with_name("quote")
            .short("q")
            .long("quote")
            .help("use given characters as quotes. Any character here will be used as opening quote and only the same character will be used as closing quote")
            .takes_value(true))
        .group(ArgGroup::with_name("quotes")
            .args(&["single", "double", "both", "quote"])
            .multiple(false))
        .arg(Arg::with_name("separator")
            .short("t")
            .long("sep")
            .takes_value(true)
            .help("Use given separator to join fields. Default |"))
        .get_matches();

    let quote = if matches.is_present("b") {
        "\"'"
    } else if matches.is_present("s") {
        "'"
    } else {
        matches.value_of("q").or(Some("\"")).unwrap()
    };

    let delimiter = matches.value_of("f").or(Some(" \t")).unwrap();
    let separator = matches.value_of("t").or(Some("|")).unwrap();

    parser::process(separator, quote, delimiter);
}
